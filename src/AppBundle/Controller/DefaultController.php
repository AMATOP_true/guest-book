<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\NoteForm;
use AppBundle\Entity\Note;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $noteRepository = $em->getRepository(Note::class);
        $notes = $noteRepository->findByDeleted(false);
        $note = new Note();
        $noteForm = $this->createForm(new NoteForm(), $note);
        if ($request->getMethod() == 'POST') {
            {
                $noteForm->handleRequest($request);
                if ($noteForm->isValid()) {
                    $em->persist($note);
                    $em->flush();
                    return $this->redirect($this->generateUrl('homepage'));
                }
            }
        }
        return $this->render('default/index.html.twig', array(
            'noteForm' => $noteForm->createView(),
            'notes' => $notes
        ));
    }
}
