<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NoteForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userName', 'text')
            ->add('email', 'email')
            ->add('message', 'textarea', array('attr' => array('rows' => '4'), 'required' => true))
            ->add('send', 'submit', array(
                'attr' => array('class' => 'submit')))
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Note',
        ));
    }

    public function getName()
    {
        return 'userForm';
    }
}