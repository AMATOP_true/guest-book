<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Entity\Note;

class NoteMarkDeletedCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('note:mark_deleted')
            ->addArgument('count', InputArgument::REQUIRED, 'Count.')
            ->setDescription('Delete old notes.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $count = $input->getArgument('count');
        $notes = $em->getRepository(Note::class)->getOldNotes($count);

        /** @var Note $note */
        foreach ($notes as $note){
            $note->setDeleted(true);
        }
        $em->flush($notes);
    }

}
