<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class NoteRepository extends EntityRepository
{
    public function getOldNotes($count)
    {
        $qb = $this->_em->createQueryBuilder();
        $select = $qb
            ->select([
                'user_note'
            ])
            ->from('AppBundle\Entity\Note', 'user_note')
            ->where('user_note.deleted= :false')
            ->orderBy('user_note.id', 'ASC')
            ->setMaxResults($count)
            ->setParameter('false', false);
        return $select->getQuery()->getResult();
    }
}