<?php

namespace AppBundle\EventListener;

use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Note;

class RegistrationListener implements EventSubscriberInterface
{
    private $router;
    private $em;

    public function __construct(UrlGeneratorInterface $router, EntityManager $em) {
        $this->router = $router; // нужен али нет?
        $this->em = $em;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_SUCCESS => array('onRegistrationSuccess', 255)
        );
    }

    /**
     * @param FormEvent $event
     */
    public function onRegistrationSuccess(FormEvent $event)
    {
        $user = $event->getForm()->getData();
        $username = $user->getUsername();
        $note = new Note();
        $note->setMessage("Приветствуем нового пользователя $username");
        $note->setUserName('admin');
        $this->em->persist($note);
        $this->em->flush();
    }
}
